<?php

namespace Jojoee\Mediumm\Templates\Sidebar;

// Disable dynamic sidebar from wordpress
// dynamic_sidebar( 'sidebar-primary' );

// Embed custom sidebar
get_template_part('templates/custom-sidebar');
?>